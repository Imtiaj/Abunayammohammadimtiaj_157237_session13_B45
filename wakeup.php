<?php

class Person
{
    public $name = "Default name";
    public $address = "Default address";
    public $phone = "Default Phone Number";
    public function __wakeup()
    {
        $this->doSomething();
    }
    public function doSomething(){
        echo "i'm in the class";
    }
}
$obj= new Person();
$myVar = serialize($obj);
echo "<br>";
var_dump($myVar);
echo "<br>";
$newObj= unserialize($myVar);
echo "<br>";
var_dump($newObj);
?>