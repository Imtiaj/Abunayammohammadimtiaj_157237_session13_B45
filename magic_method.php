<?php

class Person{

    public $name="Default name";
    public $address="Default address";
    public $phone="Default Phone Number";

    public static $mystaticproperty;

    public function __construct()
    {
        echo "i'm inside ".__METHOD__."<br>";
    }

    public function __destruct()
    {
        echo "i'm inside ".__METHOD__."<br>";
    }


    //__call magic method start
    public function __call($name, $arguments)
    {
        echo "i'm inside ".__METHOD__."<br>";
        echo "Wrong Method=$name";

        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }
    //__callstatic magic method start
    public static function __callStatic($name, $arguments)
    {
        echo "i'm inside ".__METHOD__."<br>";
        echo "Wrong Static Method=$name";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }

    public function doSomething(){
        echo "i'm in the class";
    }

    public static function doFrequnetly(){
        echo "I'm doing it frequently<br>";
    }


    //set magic method
    public function __set($name, $value)
    {
        echo "i'm inside ".__METHOD__."<br>";
        echo "Wrong Property Name=$name <br>";
        echo "Value tried to set that wrong property is = $value";
    }

    //get magic method
    public function __get($name)
    {
        echo "i'm inside ".__METHOD__."<br>";
        echo "Wrong Property Name=$name <br>";
    }

    //isset magic method
    public function __isset($name)
    {
        echo "i'm inside ".__METHOD__."<br>";
        echo "Wrong Property Name=$name <br>";
    }

    //unset magic method
    public function __unset($name)
    {
        echo "i'm inside ".__METHOD__."<br>";
        echo "Wrong Property Name=$name <br>";
        echo "<br>";
        echo "<br>";

    }

    //magic method sleep
    public function __sleep()
    {
        return array("name","phone");
    }

    //magic method wakeup
    public function __wakeup()
    {
        $this->doSomething();
    }

    //magic method tostring
    public function __toString()
    {
        return "Are you crazy! I'm just an object! Not an string!";
    }

    //magic method invoke
    public function __invoke($value)
    {
        echo "i'm inside ".__METHOD__."<br>";
        echo "value=$value.<br>";
    }
}


Person::$mystaticproperty="Hello Static";
Person::$mystaticproperty="hello";

Person::dposdaoasjdjhda("Wrong Static Parameter");
echo Person::$mystaticproperty;

$obj= new Person();

echo "hello world!<br>";

$obj->doSomet(324324,"rsfsfsdfsdfsdf");

$obj->dob="set this string.<br>";//set example
echo "<br>";



echo $obj->sadsadasd;//get example

if(isset($obj->sadsad) ) {
    //isset example
}

unset($obj->ahdkjas);//unset example
echo "<br>";


$myVar = serialize($obj);
var_dump($myVar);

echo "<br>";
echo "<br>";

$newObj= unserialize($myVar);
var_dump($newObj);
$obj(545);
echo "<br>";
echo "<br>";
$str=$obj."Get Lost";
echo $str;
echo "<br>";
echo "<br>";
echo "<br>";

?>