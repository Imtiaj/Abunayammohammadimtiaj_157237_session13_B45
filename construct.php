<?php

class Person
{

    public $name = "Default name";
    public $address = "Default address";
    public $phone = "Default Phone Number";

    public static $mystaticproperty;

    public function __construct()
    {
        echo "i'm inside " . __METHOD__ . "<br>";
    }
}
$obj= new Person();
?>